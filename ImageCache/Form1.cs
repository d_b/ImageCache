﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageCache
{
    public partial class Form1 : Form
    {
        private ImageCache _imageCache = new ImageCache();
        private List<PictureBox> _pictureBoxes = new List<PictureBox>(4);
        List<Task> pictureBoxManipulatorTasks = new List<Task>(4);
        private Random _randNumGenerator = new Random();
        private bool _canRun = false;

        public Form1()
        {
            InitializeComponent();

            _pictureBoxes.Add(pictureBox1);
            _pictureBoxes.Add(pictureBox2);
            _pictureBoxes.Add(pictureBox3);
            _pictureBoxes.Add(pictureBox4);
        }

        private ImageAccessor.Images GetRandomKey()
        {
            int randNum = _randNumGenerator.Next(7);
            ImageAccessor.Images randomKey;

            switch (randNum)
            {
                case 0:
                    randomKey = ImageAccessor.Images.Blue;
                    break;
                case 1:
                    randomKey = ImageAccessor.Images.Cyan;
                    break;
                case 2:
                    randomKey = ImageAccessor.Images.Green;
                    break;
                case 3:
                    randomKey = ImageAccessor.Images.Orange;
                    break;
                case 4:
                    randomKey = ImageAccessor.Images.Pink;
                    break;
                case 5:
                    randomKey = ImageAccessor.Images.Red;
                    break;
                case 6:
                    randomKey = ImageAccessor.Images.Yellow;
                    break;

                default:
                    randomKey = ImageAccessor.Images.Orange;
                    break;
            }

            return randomKey;
        }

        private void SetPictureBox(PictureBox pictureBox, Image image)
        {
            if (pictureBox.Image != null)
            {
                pictureBox.Image.Dispose();
            }

            pictureBox.Image = image;
            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void RunCached_Button_Click(object sender, EventArgs e)
        {
            if (!_canRun)
            {
                _canRun = true;
                State_Label.Text = "State: On";

                int i = 1;
                foreach (PictureBox pictureBox in _pictureBoxes)
                {
                    pictureBoxManipulatorTasks.Add(
                        CreateAndStartPictureBoxManipulationTask(pictureBox, i));
                    i++;
                }
            }
        }

        private async Task CreateAndStartPictureBoxManipulationTask(PictureBox pictureBox, int i)
        {
            int sleepTime = 200;
            Task t = new Task((pb) =>
            {
                Trace.WriteLine("Task for pictureBox" + i + " has started");

                while (_canRun)
                {
                    ImageAccessor.Images randomKey = GetRandomKey();
                    Image newImage = _imageCache.TryGetImage(randomKey);
                    SetPictureBox((PictureBox) pb, newImage);
                    // Trace.WriteLine("Task for pictureBox" + i " is doing work");

                    Thread.Sleep(sleepTime);
                }

                SetPictureBox((PictureBox) pb, null);
                Trace.WriteLine("Task for pictureBox" + i + " has ended");
            }, pictureBox);

            t.Start();
            await t;
        }

        private void Reset_Button_Click_1(object sender, EventArgs e)
        {
            if (_canRun)
            {
                // makes the tasks stop
                _canRun = false;

                // clear cache and pictureBoxes
                _imageCache.Clear();

                State_Label.Text = "State: Off";
            }
        }
    }
}
