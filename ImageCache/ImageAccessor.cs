﻿using System.Drawing;
using System.Threading;

namespace ImageCache
{
    // ImageAccessor acts as a Pseudo-Database
    public class ImageAccessor
    {
        public static int BlueRequests { get; private set; }
        public static int CyanRequests { get; private set; }
        public static int GreenRequests { get; private set; }
        public static int OrangeRequests { get; private set; }
        public static int PinkRequests { get; private set; }
        public static int RedRequests { get; private set; }
        public static int YellowRequests { get; private set; }

        public enum Images
        {
            Blue,
            Cyan,
            Green,
            Orange,
            Pink,
            Red,
            Yellow
        }

        private static readonly string DirectoryPath = System.AppContext.BaseDirectory + @"Images\";

        public ImageAccessor()
        {
            BlueRequests = 0;
            CyanRequests = 0;
            GreenRequests = 0;
            OrangeRequests = 0;
            PinkRequests = 0;
            RedRequests = 0;
            YellowRequests = 0;
        }

        public Image GetBlueImage()
        {
            BlueRequests++;
            SimulateSlowness();
            return Image.FromFile(DirectoryPath + "blue.png");
        }

        public Image GetCyanImage()
        {
            CyanRequests++;
            SimulateSlowness();
            return Image.FromFile(DirectoryPath + "cyan.png");
        }

        public Image GetGreenImage()
        {
            GreenRequests++;
            SimulateSlowness();
            return Image.FromFile(DirectoryPath + "green.png");
        }
        
        public Image GetOrangeImage()
        {
            OrangeRequests++;
            SimulateSlowness();
            return Image.FromFile(DirectoryPath + "orange.png");
        }

        public Image GetPinkImage()
        {
            PinkRequests++;
            SimulateSlowness();
            return Image.FromFile(DirectoryPath + "pink.png");
        }

        public Image GetRedImage()
        {
            RedRequests++;
            SimulateSlowness();
            return Image.FromFile(DirectoryPath + "red.png");
        }

        public Image GetYellowImage()
        {
            YellowRequests++;
            SimulateSlowness();
            return Image.FromFile(DirectoryPath + "yellow.png");
        }

        private void SimulateSlowness()
        {
            // simulate that the database takes a tremendous amount of time
            // to get the requested image
            long milliseconds = 1000;
            Thread.Sleep(1000);
        }
    }
}