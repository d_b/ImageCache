﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;

namespace ImageCache
{
    public class ImageCache
    {
        private Dictionary<ImageAccessor.Images, Image> _cache = new Dictionary<ImageAccessor.Images, Image>();
        private ImageAccessor _database = new ImageAccessor();
        private ReaderWriterLockSlim _rwLock = new ReaderWriterLockSlim();

        public ImageCache()
        {

        }

        public void Clear()
        {
            _rwLock.EnterWriteLock();
            try
            {
                foreach (Image image in _cache.Values)
                {
                    image.Dispose();
                }
                _cache.Clear();
            }
            finally
            {
                _rwLock.ExitWriteLock();
            }

            Trace.WriteLine("Cache cleared");
        }

        public Image TryGetImage(ImageAccessor.Images key)
        {
            Image foundImage = null;
            Image clonedFoundImage = null;
            bool cacheContainsKey;
            
            _rwLock.EnterReadLock();
            try
            {
                cacheContainsKey = _cache.ContainsKey(key);
            }
            finally
            {
                _rwLock.ExitReadLock();
            }
             
            if (cacheContainsKey)
            {
                foundImage = ReadFromCache(key);
            }
            else
            {
                foundImage = ReadFromDatabase(key);
            }

            lock (foundImage)
            {
                clonedFoundImage = (Image) foundImage.Clone();
            }

            // only return a clone of the image in _cache
            return clonedFoundImage;
        }

        private Image ReadFromCache(ImageAccessor.Images key)
        {
            return _cache[key];
        }

        private Image ReadFromDatabase(ImageAccessor.Images key)
        {
            Image foundImage = null;

            _rwLock.EnterWriteLock();
            try
            {
                switch (key)
                {
                    case ImageAccessor.Images.Blue:
                        foundImage = _database.GetBlueImage();
                        break;
                    case ImageAccessor.Images.Cyan:
                        foundImage = _database.GetCyanImage();
                        break;
                    case ImageAccessor.Images.Green:
                        foundImage = _database.GetGreenImage();
                        break;
                    case ImageAccessor.Images.Orange:
                        foundImage = _database.GetOrangeImage();
                        break;
                    case ImageAccessor.Images.Pink:
                        foundImage = _database.GetPinkImage();
                        break;
                    case ImageAccessor.Images.Red:
                        foundImage = _database.GetRedImage();
                        break;
                    case ImageAccessor.Images.Yellow:
                        foundImage = _database.GetYellowImage();
                        break;

                    default:
                        foundImage = _database.GetOrangeImage();
                        break;
                }
                // add image to cache if it's not yet contained by it
                if (!_cache.ContainsKey(key))
                {
                    _cache.Add(key, foundImage);
                    Trace.WriteLine("Added " + key.ToString() + " to cache");
                }
            }
            finally
            {
                _rwLock.ExitWriteLock();
            }

            return foundImage;
        }
    }
}